export interface IProduct  {
  product_id: string;
  name: string;
  price: number;
  image: string;
  description?: string;
}
