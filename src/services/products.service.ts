import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map, Observable} from 'rxjs';
import {IProduct} from '../interfaces/product.interface';

interface productListResponse {
  products: IProduct[]
}

@Injectable()
export class ProductsService {
  private baseURL = 'https://s3-eu-west-1.amazonaws.com/developer-application-test/cart';

  constructor(private http: HttpClient) {}

  getProducts(): Observable<IProduct[]> {
    return this.http.get<productListResponse>(`${this.baseURL}/list`).pipe(map((response: productListResponse) => response.products))
  }

  getProduct(id: string): Observable<IProduct> {
    return this.http.get<IProduct>(`${this.baseURL}/${id}/detail`)
  }
}
