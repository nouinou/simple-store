import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {ProductsService} from './products.service';
import {IProduct} from '../interfaces/product.interface';
import {of, throwError} from 'rxjs';

describe('ProductsService', () => {
  let service: ProductsService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;
  const productsStub: IProduct[] = [
    {
      product_id: '1',
      name: 'Apples',
      price: 120,
      image: 'https://s3-eu-west-1.amazonaws.com/developer-application-test/images/1.jpg'
    },
    {
      product_id: '2',
      name: 'Oranges',
      price: 167,
      image: 'https://s3-eu-west-1.amazonaws.com/developer-application-test/images/2.jpg'
    },
    {
      product_id: '3',
      name: 'Bananas',
      price: 88,
      image: 'https://s3-eu-west-1.amazonaws.com/developer-application-test/images/3.jpg'
    }];

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    service = new ProductsService(httpClientSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('getProducts', () => {
    it('should return expected products', (done: DoneFn) => {
      httpClientSpy.get.and.returnValue(of({products: productsStub}));

      service.getProducts().subscribe({
        next: product => {
          expect(product).toEqual(productsStub);
          expect(product).toEqual(productsStub);
          done();
        },
        error: done.fail
      });

      expect(httpClientSpy.get.calls.count())
        .withContext('one call')
        .toBe(1);
    });

    it('should return an error when the server returns a 404', (done: DoneFn) => {
      const errorResponse = new HttpErrorResponse({
        error: '404 Not Found',
        status: 404, statusText: 'Not Found'
      });

      httpClientSpy.get.and.returnValue(throwError(() => errorResponse));

      service.getProducts().subscribe({
        next: () => done.fail('expected an error, not products'),
        error: error => {
          expect(error.message).toContain('404 Not Found');
          done();
        }
      });
    });
  })

  describe('getProduct', () => {
    it('should return expected product', (done: DoneFn) => {
      httpClientSpy.get.and.returnValue(of(productsStub[0]));

      service.getProduct('someId').subscribe({
        next: product => {
          expect(product).toEqual(productsStub[0]);
          done();
        },
        error: done.fail
      });

      expect(httpClientSpy.get.calls.count())
        .withContext('one call')
        .toBe(1);
    });

    it('should return an error when the server returns a 404', (done: DoneFn) => {
      const errorResponse = new HttpErrorResponse({
        error: '404 Not Found',
        status: 404, statusText: 'Not Found'
      });

      httpClientSpy.get.and.returnValue(throwError(() => errorResponse));

      service.getProduct('someId').subscribe({
        next: () => done.fail('expected an error, not products'),
        error: error => {
          expect(error.message).toContain('404 Not Found');
          done();
        }
      });
    });
  })
});
