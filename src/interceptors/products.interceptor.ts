import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {map, Observable} from 'rxjs';
import {camelCase, map as lodashMap, mapKeys} from 'lodash';
import {IProduct} from '../interfaces/product.interface';

@Injectable()
export class ProductsInterceptor implements HttpInterceptor {
  intercept(httpRequest: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // add baseUrl
    const httpsReq = httpRequest.clone({
      url: 'https://s3-eu-west-1.amazonaws.com/developer-application-test/cart/' + httpRequest.url
    });

    // Change the response object product_id to productId
    return next.handle(httpsReq).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          let responseBody: any;

          if (event.url?.includes('/list')) {
            const camelCasedArray = lodashMap(event.body.products, (product: IProduct) => mapKeys(product, (value, key) => camelCase(key)));
            responseBody = {...event.body, products: camelCasedArray}
          } else {
            responseBody = mapKeys(event.body, (value, key) => camelCase(key));
          }

          const eventClone = event.clone({body: responseBody});
          console.log(eventClone.body);

          return eventClone;
        }

        return event;
      })
    );
  }
}
