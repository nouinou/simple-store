import {ComponentFixture, TestBed} from '@angular/core/testing';
import {ProductDetailsComponent} from './product-details.component';
import {ProductsService} from '../../services/products.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ActivatedRoute} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {MatIconModule} from '@angular/material/icon';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatButtonModule} from '@angular/material/button';
import {Observable, of} from 'rxjs';
import {Component} from '@angular/core';
import {IProduct} from '../../interfaces/product.interface';

const PRODUCT = {
  product_id: '1',
  name: 'Apples',
  price: 120,
  image: 'https://s3-eu-west-1.amazonaws.com/developer-application-test/images/1.jpg'
};

describe('ProductDetailsComponent', () => {
  let component: ProductDetailsComponent;
  let fixture: ComponentFixture<ProductDetailsComponent>;
  let productsService: ProductsService;
  let route: ActivatedRoute;
  let testComponentWrapper: TestComponentWrapper;
  let hostFixture: ComponentFixture<TestComponentWrapper>;
  let spy: jasmine.Spy<(id: string) => Observable<IProduct>>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProductDetailsComponent, TestComponentWrapper],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        MatIconModule,
        MatProgressSpinnerModule,
        MatButtonModule
      ],
      providers: [ProductsService]
    }).compileComponents();

    productsService = TestBed.inject(ProductsService);
    route = TestBed.inject(ActivatedRoute);

    spy = spyOn(productsService, 'getProduct').and.returnValue(of(PRODUCT));
  });

  it('should show product details from input', () => {
    hostFixture = TestBed.createComponent(TestComponentWrapper);
    testComponentWrapper = hostFixture.componentInstance;
    hostFixture.detectChanges();

    const nativeEl = hostFixture.debugElement.nativeElement;

    expect(nativeEl.querySelector('.name')?.textContent).toEqual(PRODUCT.name)
    expect(nativeEl.querySelector('.price')?.textContent).toEqual('€' + PRODUCT.price);
  });

  it('should get productId from route', () => {
    const spy = spyOn(route.snapshot.paramMap, 'get').and.returnValue(PRODUCT.product_id);
    fixture = TestBed.createComponent(ProductDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges()

    expect(spy).toHaveBeenCalled()
  })
});

@Component({
  template: '<app-product-details [productId]="productId"></app-product-details>'
})
class TestComponentWrapper {
  productId = PRODUCT.product_id;
}
