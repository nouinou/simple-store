import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {IProduct} from '../../interfaces/product.interface';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';
import {ProductsService} from '../../services/products.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductDetailsComponent implements OnInit, OnDestroy {
  @Input() productId!: string;
  product?: IProduct;
  isLoading = true;
  subscription = new Subscription();
  onDialog = false;

  constructor(
    private productsService: ProductsService,
    private route: ActivatedRoute,
    private cdRef: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.getProductId();
    this.getProduct();
  }

  getProductId(): void {
    this.onDialog = !!this.productId;
    this.productId = this.productId || this.route.snapshot.paramMap.get('id')!;
  }

  getProduct(): void {
    this.subscription.add(
      this.productsService.getProduct(this.productId).subscribe({
          next: (product: IProduct) => {
            this.product = product;
            this.showLoadingComplete();
          },
          error: () => {
            this.showLoadingComplete();
          }
        }
      ))
  }

  showLoadingComplete(): void {
    this.isLoading = false;
    this.cdRef.detectChanges();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
