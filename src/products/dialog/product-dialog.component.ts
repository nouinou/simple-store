import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-product-dialog',
  template: `
    <mat-icon class="close-icon"
              (click)="dialogRef.close()">
      close
    </mat-icon>
    <app-product-details [productId]="id"></app-product-details>`,
  styleUrls: ['./product-dialog.component.scss']
})
export class ProductDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<ProductDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public id: string,
  ) {}
}
