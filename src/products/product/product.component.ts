import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {IProduct} from '../../interfaces/product.interface';
import {ProductDialogComponent} from '../dialog/product-dialog.component';
import {DeviceDetectorService} from 'ngx-device-detector';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';

const DIALOG_WIDTH = '500px';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductComponent {
  @Input() product!: IProduct;

  constructor(
    private deviceService: DeviceDetectorService,
    private router: Router,
    private route: ActivatedRoute,
    public dialog: MatDialog
  ) {}

  isDesktop(): boolean {
    return this.deviceService.isDesktop();
  }

  showProductDetails(id: string): void {
    if (this.isDesktop()) {
      this.dialog.open<ProductDialogComponent>(ProductDialogComponent, {
        data: id,
        width: DIALOG_WIDTH
      })
    } else {
      this.router.navigate([id], {relativeTo: this.route});
    }
  }
}
