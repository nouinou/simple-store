import {ProductComponent} from './product.component';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {DeviceDetectorService} from 'ngx-device-detector';
import {ActivatedRoute, Router} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {MatDialogModule} from '@angular/material/dialog';
import {Component} from '@angular/core';

const PRODUCT = {
  product_id: '1',
  name: 'Apples',
  price: 120,
  image: 'https://s3-eu-west-1.amazonaws.com/developer-application-test/images/1.jpg'
};

describe('ProductComponent', () => {
  let component: ProductComponent;
  let fixture: ComponentFixture<ProductComponent>;
  let deviceService: DeviceDetectorService
  let router: Router;
  let route: ActivatedRoute;
  let testComponentWrapper: TestComponentWrapper;
  let hostFixture: ComponentFixture<TestComponentWrapper>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatDialogModule
      ],
      declarations: [ProductComponent, TestComponentWrapper]
    }).compileComponents();

    router = TestBed.inject(Router);
    route = TestBed.inject(ActivatedRoute);
    deviceService = TestBed.inject(DeviceDetectorService);
    fixture = TestBed.createComponent(ProductComponent);
    component = fixture.componentInstance;
    hostFixture = TestBed.createComponent(TestComponentWrapper);
    testComponentWrapper = hostFixture.componentInstance;
    hostFixture.detectChanges();
  });

  it('should show image, name and the price of the product', () => {
    const nativeEl = hostFixture.debugElement.nativeElement;

    const name = nativeEl.querySelector('.name').textContent;
    const imageSrc = nativeEl.querySelector('.image');
    const price = nativeEl.querySelector('.price').textContent;

    expect(name).toEqual(PRODUCT.name);
    expect(imageSrc).not.toBeNull();
    expect(price).toEqual('€' + PRODUCT.price);
  })


  describe('Show Product Details', () => {
    it('should navigate to /list/:productId', async () => {
      const navigateSpy = spyOn(router, 'navigate');
      spyOn(deviceService, 'isDesktop').and.returnValue(false)
      hostFixture = TestBed.createComponent(TestComponentWrapper);
      testComponentWrapper = hostFixture.componentInstance;
      hostFixture.detectChanges();
      const nativeEl = hostFixture.debugElement.nativeElement;

      nativeEl.querySelector('.product').click();

      expect(navigateSpy).toHaveBeenCalledWith(['1'], {relativeTo: route});
    });

    it('should show dialog', async () => {
      const openDialogSpy = spyOn(component.dialog, 'open')
      spyOn(deviceService, 'isDesktop').and.returnValue(true)

      component.showProductDetails('1');

      expect(openDialogSpy).toHaveBeenCalled();
    });
  })
});

@Component({
  template: '<app-product [product]="products"></app-product>'
})
class TestComponentWrapper {
  products = PRODUCT;
}
