import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProductsService} from '../services/products.service';
import {debounceTime, distinctUntilChanged, Subscription} from 'rxjs';
import {IProduct} from '../interfaces/product.interface';
import {DeviceDetectorService, DeviceType} from 'ngx-device-detector';
import {FormControl} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';

const DESKTOP_AND_MOBILE_HEIGHT = '220px';
const TABLET_HEIGHT = '280px';
const DEBOUNCE_TIME = 300;

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit, OnDestroy {
  products?: IProduct[];
  filteredProducts?: IProduct[];
  subscription: Subscription = new Subscription();
  isLoading = true;
  cols = 2;
  rowHeight = DESKTOP_AND_MOBILE_HEIGHT;
  searchInputControl: FormControl<string> = new FormControl();

  constructor(
    private productsService: ProductsService,
    private deviceService: DeviceDetectorService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.calculateGridParams();
    this.getProducts();
    this.subscribeToSearch();
  }

  getDeviceType(): string {
    return this.deviceService.deviceType;
  }

  calculateGridParams(): void {
    this.cols = this.getDeviceType() === DeviceType.Desktop ? 6 : this.getDeviceType() === DeviceType.Tablet ? 3 : 2;
    this.rowHeight = this.cols === 3 ? TABLET_HEIGHT : DESKTOP_AND_MOBILE_HEIGHT;
  }

  getProducts(): void {
    this.subscription.add(
      this.productsService.getProducts().subscribe({
        next: (products: IProduct[]) => {
          this.products = products;
          this.isLoading = false;
          this.filteredProducts = this.products;
        },
        error: () => {
          this.isLoading = false
        }
      }))
  }

  subscribeToSearch(): void {
    this.subscription.add(
      this.searchInputControl.valueChanges
        .pipe(debounceTime(DEBOUNCE_TIME), distinctUntilChanged())
        .subscribe((productName: string) => {
          const trimmedProductName = this.trimAndLowerCase(productName);
          this.filteredProducts = this.filterProducts(trimmedProductName);
        }));
  }

  trimAndLowerCase(productName: string): string {
    return productName?.trim().toLowerCase();
  }

  filterProducts(productName: string): IProduct[] | undefined {
    if (!productName) {
      return this.products;
    }

    const regex = new RegExp(productName, 'i');
    return this.products?.filter((product: IProduct) => regex.test(product.name.toLowerCase()))
  }

  clearFilter(): void {
    this.searchInputControl.reset();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
