import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {ProductsComponent} from './products.component';
import {ProductsService} from '../services/products.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {RouterTestingModule} from '@angular/router/testing';
import {TestbedHarnessEnvironment} from '@angular/cdk/testing/testbed';
import {HarnessLoader} from '@angular/cdk/testing';
import {MatGridListHarness} from '@angular/material/grid-list/testing';
import {of} from 'rxjs';
import {IProduct} from '../interfaces/product.interface';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DeviceDetectorService, DeviceType} from 'ngx-device-detector';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatInputHarness} from '@angular/material/input/testing';
import {MatButtonHarness} from '@angular/material/button/testing';
import {MatDialogModule} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductComponent} from './product/product.component';

describe('ProductsComponent', () => {
  let component: ProductsComponent;
  let fixture: ComponentFixture<ProductsComponent>;
  let httpClient: HttpClient;
  let productsService: ProductsService
  let deviceService: DeviceDetectorService
  let loader: HarnessLoader;
  let router: Router;
  let route: ActivatedRoute;
  const productsStub: IProduct[] = [
    {
      product_id: '1',
      name: 'Apples',
      price: 120,
      image: 'https://s3-eu-west-1.amazonaws.com/developer-application-test/images/1.jpg'
    },
    {
      product_id: '2',
      name: 'Oranges',
      price: 167,
      image: 'https://s3-eu-west-1.amazonaws.com/developer-application-test/images/2.jpg'
    },
    {
      product_id: '3',
      name: 'Bananas',
      price: 88,
      image: 'https://s3-eu-west-1.amazonaws.com/developer-application-test/images/3.jpg'
    }];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        MatGridListModule,
        MatProgressSpinnerModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        MatButtonModule,
        MatDialogModule,
        BrowserAnimationsModule
      ],
      declarations: [ProductsComponent, ProductComponent],
      providers: [ProductsService]
    }).compileComponents();

    fixture = TestBed.createComponent(ProductsComponent);
    httpClient = TestBed.inject(HttpClient);
    productsService = TestBed.inject(ProductsService);
    deviceService = TestBed.inject(DeviceDetectorService);
    component = fixture.componentInstance;
    loader = TestbedHarnessEnvironment.loader(fixture);
    router = TestBed.inject(Router);
    route = TestBed.inject(ActivatedRoute);
    component = fixture.componentInstance;

    spyOn(productsService, 'getProducts').and.returnValue(of(productsStub));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Project List', () => {
    it('should render product list', async () => {
      const gridList = await loader.getHarness<MatGridListHarness>(MatGridListHarness);
      expect(gridList).not.toBeNull();
    });

    it('should show 2 cols | mobile', async () => {
      deviceService.deviceType = DeviceType.Mobile;
      const gridList = await loader.getHarness<MatGridListHarness>(MatGridListHarness);
      const columnLength = await gridList.getColumns();

      expect(columnLength).toEqual(2)
    });

    it('should show a grid of 3 column | tablet', async () => {
      deviceService.deviceType = DeviceType.Tablet;
      const gridList = await loader.getHarness<MatGridListHarness>(MatGridListHarness);
      const columnLength = await gridList.getColumns();

      expect(columnLength).toEqual(3)
    });

    it('should show a grid of 6 column | desktop', async () => {
      deviceService.deviceType = DeviceType.Desktop;
      const gridList = await loader.getHarness<MatGridListHarness>(MatGridListHarness);
      const columnLength = await gridList.getColumns();

      expect(columnLength).toEqual(6)
    });
  })

  describe('Filter Products', () => {
    it('should filter products', fakeAsync(async () => {
      const expectedProductName = productsStub[1].name; // Oranges
      const input = await loader.getHarness<MatInputHarness>(MatInputHarness);

      await input.setValue('ora');
      tick(400);
      fixture.detectChanges();

      const productList = fixture.debugElement.nativeElement.querySelectorAll('.product');
      const result = productList[0].querySelector('.name').textContent;

      expect(productList.length).toBe(1);
      expect(result).toEqual(expectedProductName);
    }));

    it('should clear the filter', fakeAsync(async () => {
      const input = await loader.getHarness<MatInputHarness>(MatInputHarness);

      await input.setValue('ora');
      tick(400);
      const closeButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({selector: '.close-button'}));
      await closeButton.click();
      tick(400);
      fixture.detectChanges();

      const productList = fixture.debugElement.nativeElement.querySelectorAll('.product');

      expect(productList.length).toBe(3);
    }));
  })
});
