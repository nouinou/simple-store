import {Component} from '@angular/core';

const TITLE = 'Simple Store'
@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent {
  title = TITLE;
}
