# Simple Store

This is a small SPA that does the following:

1. A list of products is retrieved from a REST endpoint.
2. This list of products is displayed using a responsive Grid with the following requirements:
   * URL: `/list`
   * Mobile device: 2 items per row
   * Tablet device: 3 items per row
   * Desktop: 6 items per row
3. Each product item contains the following information:
   * Name
   * Price
   * Image
4. The list of products can be filtered, by typing a query in a text field at the top of the list.
5. A product container is a clickable / tappable element, which has the following logic:
   * If the user is on mobile device, it redirects to a separate view with product details. URL: `/list/productID`
   * If the user is on desktop, it triggers a “popup/lightbox” view with product details.
6. The details of the clicked product are retrieved from a REST endpoint as well.
7. A User can also access product pages on the desktop directly by going to `/list/productID`

## Libraries used:
* [ngx-device-detector](https://github.com/AhsanAyaz/ngx-device-detector): Used to detect the device type. I chose it because of it's simple to use and constantly supported.
* [Angular Material](https://material.angular.io/): Used because it offers customizable UI elements that facilitate development. Especially the product-list grid and the product dialog component.

## App architecture
This is a simple Angular application that contains two modules:
  1. `AppModule`: this is the root module that contains `AppComponent`. It contains:
     * TopBarComponent: used to display a top navigation bar using Angular materials' `mat-toolbar`
     * Router@utlet : The built-in Angular directive that is used to display the content of the current route. It is set to route to `/list` , which will import and use the second module.
  2. `ProductsModule`: it will lazily load when visiting `/list`. It holds 4 components and is provided by 1 service.
     * `ProductsService`: it provides methods for fetching product data from a backend server.
     * `ProductsComponent`: it subscribes to `getProducts()` from the service and renders the product list. It includes the search input for filtering the products.
     * `ProductComponent`: it's a presentational child component that receives a single product's data using property binding. It
       handles a click/tap on a single product depending on the device type.
     * `ProductDialogComponent`: it's a dumb component used with angular material's dialog and holds ProductDetailsComponent as a child component.
     * `ProductDetailsComponent`: this one is accessed to via dialog and received the `product_id` using property binding or navigated to via `/list/:id`. It subscribes to the `getProduct` method of the service and renders the product details.

## Screenshots
### Desktop view
![desktop.png](screenshots%2Fdesktop.png)

### Tablet view
<img src="screenshots/tablet.png" width="50%">

### Mobile view
<img src="screenshots/mobile.png" width="40%">


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
